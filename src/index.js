import React from 'react';
import ReactDOM from 'react-dom';
import App from "./App";

// const element = <div> countdown champ </div>;

ReactDOM.render(
    <App />,
    document.getElementById('root')
);