import React, {Component} from 'react';
import Clock from './Clock';
import '../public/App.css';
import { Form, FormControl, Button} from 'react-bootstrap';
import Stopwatch from "./Stopwatch";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            deadline: 'November 25, 2017',
            newDeadline: '',
            endingTime: 15,
            newEndingTime: ''
        }
    }
    changeDeadline() {
        // this.setState( {deadline: 'November 25, 2017'})
        // console.log('state', this.state);
        this.setState({deadline: this.state.newDeadline});
    }
    changeEndingTime() {
        this.setState({endingTime: this.state.newEndingTime});
    }

    render() {
        return (
            <div className="App">
                <div className="App-title">
                    Countdown to {this.state.deadline}
                </div>
                <Clock
                    deadline={this.state.deadline}
                />
                <Form inline>
                    <FormControl
                        className="Deadline-input"
                        placeholder='new date'
                        onChange={event => this.setState({newDeadline: event.target.value})}
                    />
                    <Button onClick={() => this.changeDeadline()}>
                        Submit
                    </Button>
                </Form>
                <div className="Second-App-Title">
                    Stopwatch: {this.state.endingTime}
                </div>
                <Stopwatch
                    endingTime={this.state.endingTime}
                />
               <Form inline>
                   <FormControl
                       className="Stopwatch-input"
                       placeholder="new time"
                       onChange={event => this.setState ({ newEndingTime: parseInt(event.target.value)})}
                   />
                   <Button onClick={() => this.changeEndingTime()}>
                       Submit
                   </Button>
               </Form>
            </div>
        )
    }
}

export default App;