import React, { Component } from 'react';
import '../public/App.css'

class Stopwatch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            time: this.props.endingTime,
        }
    }

    componentWillMount() {
        this.getTimeUntil(this.state.time);
    }
    componentDidMount() {
        setInterval(() => this.getTimeUntil(this.state.time), 1000);
    }
    componentWillReceiveProps(nextProps) {
        this.setState( { time: nextProps.endingTime });
    }

    getTimeUntil(t) {
        if(t > 0) {
            this.setState({time: --t});
        } 
    }
    render() {
        return (
            <div className="Stopwatch-time">{this.state.time} seconds</div>
        )
    }
}
export default Stopwatch;